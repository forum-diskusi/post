from sqlalchemy import Column, Integer, String, create_engine, select, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.sql.sqltypes import DateTime


engine = create_engine('mysql+mysqlconnector://admin:admin123@localhost:3306/tugasakhir2', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")

Base = declarative_base()

class user(Base):
   __tablename__ = 'user_account'
   userid = Column(Integer, primary_key =  True)
   username = Column(String)
   email = Column(String)

class post(Base):
   __tablename__ = 'user_post'
   postid = Column(Integer, primary_key =  True)
   post = Column(String)
   createdate = Column(DateTime)
   userid = Column(Integer, ForeignKey(user.userid))

class reply(Base):
   __tablename__ = 'user_reply'
   replyid = Column(Integer, primary_key =  True)
   reply = Column(String)
   createdate = Column(DateTime)
   postid = Column(Integer, ForeignKey(post.postid))
   userid = Column(Integer, ForeignKey(user.userid))
