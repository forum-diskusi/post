# FORUM DISKUSI
Ini merupakan backend dari sebuah aplikasi forum diskusi. Kita bisa mendapat data berupa json, mengubah, menambah, dan menghapus data melalui route. Backend dibagi menjadi dua:
### 1. post<br>
post menggunakan framework flask dan database mysql
### 2. profile<br>
profile menggunakan framework fastapi dan database mongodb

## Cara run
1. siapkan virtual environment python
    - masuk kedalam virtual environment python
    - install semua requirenment.txt
    - pip install -r requirements.txt
2. siapkan database
    - aplikasi ini menggunakan dua jenis basis data, mysql dan mongodb
    - untuk mysql, import tugasakhir.sql
    - untuk mongodb, buat database dengan nama 'forumDiskusi' dan buat collection dengan nama 'profiles' lalu import file tugasakhir.json
    - 
3. run kedua APP
    - untuk flask, ketikan 'python main.py'
    - untuk fastapi, ketikan 'uvicorn main:app --reload'

## Route

#### 1. route user (akses terhadap data user)

- localhost:5000/api/users
    - GET = untuk menampilkan semua user
    - POST = untuk menambah user

- localhost:5000/api/user/\<username>
    - GET = untuk menampilkan data user berdasarkan input berupa username
    - PUT = untuk mengupdate data user berdasarkan input berupa username
    - DELETE = untuk menghapus data user berdasarkan input berupa username

#### 2. route post (akses terhadap data post)

- localhost:5000/api/users/\<username>/posts
    - GET = untuk menampilkan semua post dari satu user sesuai username
    - POST = untuk menambah post pada user sesuai username

- localhost:5000/api/user/\<username>/post/\<postid>
    - GET = untuk menampilkan data user berdasarkan input berupa username
    - PUT = untuk mengupdate data user berdasarkan input berupa username
    - DELETE = untuk menghapus data user berdasarkan input berupa username

#### 3. route profile (akses terhadap data profil)

- localhost:5000/profiles
    - GET = untuk menampilkan semua profile
    - POST = untuk menambah profile

- localhost:5000/profile/\{userid} 
    - GET = Menampilkan profile berdasarkan user id
    - PUT = mengubah data profil berdasarkan user id
    - DELETE = Menghapus data profil berdasarkan user id


