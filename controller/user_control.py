from models.user import *
from flask import jsonify, request
import requests

def showUsers():
    query =  session.query(user).all()
    result = {
        'users' : []
    }
    for rowUser in query:
        account = {
            'userid' : rowUser.userid,
            'username' : rowUser.username,
            'email' : rowUser.email
        }
        result['users'].append(account)
    return jsonify(result)

def insertUser():
    data = request.json
    session.add(user(**data))
    session.commit()
    result = {"massage" : "user berhasil ditambahkan"}
    return jsonify(result)

def showUserByUsername(param):
    query =  session.query(user).filter(user.username==param).all()
    if query == []:
        account = {
            'massage' : 'akun tidak ditemukan'
        }
    else:
        for rowUser in query:
            data = requests.get('http://localhost:8000/profile/{0}'.format(rowUser.userid))
            datajson = data.json()
            account = {
                'user_id' : rowUser.userid,
                'user_username' : rowUser.username,
                'user_email' : rowUser.email,
                'user_posts' : [],
                'user_profile' : [datajson]
            }
            query_post = session.query(post).filter(post.userid==rowUser.userid).all()
            for rowPost in query_post:
                posts = {
                    'post_content' : rowPost.post,
                    'post_date' : rowPost.createdate
                }
                account['user_posts'].append(posts)
    return jsonify(account)

def deleteUserByUsername(param):
    query = session.query(user).filter(user.username==param).one()
    session.delete(query)
    session.commit()
    result = {"massage" : "user berhasil dihapus"}
    return jsonify(result)


def updateUserByUsername(param):
    data = request.json
    session.query(user).filter(user.username==param).update(data)
    session.commit()
    result = {"massage" : "user berhasil diupdate"}
    return jsonify(result)