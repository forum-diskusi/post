import datetime
from models.user import *
from flask import jsonify, request
import datetime

def showPostByUsername(param):    
    query = session.query(post, user).join(user).filter(user.username==param).all()
    result = []
    for rowPost, rowUser in query:
        query_reply = session.query(reply).filter(reply.postid==rowPost.postid).count()
        posts = {
            'post_username' : rowUser.username,
            'post_id' : rowPost.postid,
            'post_content' : rowPost.post,
            'post_date' : rowPost.createdate,
            'post_totalReply' : query_reply
        }
        result.append(posts)
    return jsonify(result)

def insertPostByUsername(param):
    data = request.json
    query = session.query(user).filter(user.username==param).all()
    for rowUser in query:
        data['userid'] = rowUser.userid
        data['createdate'] = datetime.now()
    session.add(post(**data))
    session.commit()
    result = {"massage" : "post berhasil ditambahkan"}
    return jsonify(result)

def showPostByPostid(username, id):
    query = session.query(post, user).join(user).filter(user.username==username, post.postid==id).all()
    for rowPost, rowUser in query:
        result = {
            "post_username" : rowUser.username,
            "post_id" : rowPost.postid,
            "post_content" : rowPost.post,
            "post_date" : rowPost.createdate,
            "post_reply" : []
        }
        query_reply = session.query(reply, user).join(user).filter(reply.postid==rowPost.postid).all()
        for rowReply, rowUserReply in query_reply:
            replies = {
                "reply_content" : rowReply.reply,
                "reply_date" : rowReply.createdate,
                "reply_username" : rowUserReply.username
            }
            result['post_reply'].append(replies)
    return  jsonify(result)
def updatePostByPostid(id):
    query = session.query(post).filter(post.postid==id).one()
    session.delete(query)
    session.commit()
    result = {"massage" : "post berhasil dihapus"}
    return jsonify(result)
def deletePostByPostid(id):
    data = request.json
    session.query(post).filter(post.postid==id).update(data)
    session.commit()
    result = {"massage" : "post berhasil diupdate"}
    return jsonify(result)