from flask import Flask
from routes.user_route import *
from routes.post_route import *

app = Flask(__name__)
app.register_blueprint(route_user)
app.register_blueprint(route_post)

if __name__ == "__main__":
    app.run(debug=True)