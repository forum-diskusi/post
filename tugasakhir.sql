-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: tugasakhir2
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_account` (
  `userid` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES (1,'test1','test1@test.com'),(2,'test2','test2@test.com'),(3,'test3','test3@test.com'),(4,'test4','test4@test.com'),(5,'test5','test5@test.com'),(7,'test6','test6@test.com');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_post`
--

DROP TABLE IF EXISTS `user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_post` (
  `postid` int NOT NULL AUTO_INCREMENT,
  `post` varchar(255) NOT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userid` int DEFAULT NULL,
  PRIMARY KEY (`postid`),
  KEY `userid` (`userid`),
  CONSTRAINT `user_post_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user_account` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_post`
--

LOCK TABLES `user_post` WRITE;
/*!40000 ALTER TABLE `user_post` DISABLE KEYS */;
INSERT INTO `user_post` VALUES (1,'ini post pertama saya','2021-08-07 08:45:53',1),(2,'ini post kedua saya','2021-08-07 09:01:33',1),(3,'ini post ketiga saya','2021-08-07 09:01:39',1),(4,'Hai! ini adalah post pertama saya','2021-08-07 09:01:57',2),(5,'Hallo! ini merpuakan post pertama saya','2021-08-07 09:02:10',3),(6,'Hallo! ini merpuakan post kedua saya','2021-08-07 09:02:18',3),(7,'Hallo semua! ini post pertama saya','2021-08-07 09:02:43',5),(10,'hai ini percobaan post kedua saya!','2021-08-07 21:03:46',1);
/*!40000 ALTER TABLE `user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_reply`
--

DROP TABLE IF EXISTS `user_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_reply` (
  `replyid` int NOT NULL AUTO_INCREMENT,
  `reply` varchar(255) NOT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userid` int DEFAULT NULL,
  `postid` int DEFAULT NULL,
  PRIMARY KEY (`replyid`),
  KEY `userid` (`userid`),
  KEY `postid` (`postid`),
  CONSTRAINT `user_reply_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user_account` (`userid`),
  CONSTRAINT `user_reply_ibfk_2` FOREIGN KEY (`postid`) REFERENCES `user_post` (`postid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_reply`
--

LOCK TABLES `user_reply` WRITE;
/*!40000 ALTER TABLE `user_reply` DISABLE KEYS */;
INSERT INTO `user_reply` VALUES (1,'ini reply pertama saya','2021-08-07 08:48:36',2,1),(2,'hai juga! kerja bagus','2021-08-07 09:04:41',5,1),(3,'hai juga! semangat terus','2021-08-07 09:04:59',1,3);
/*!40000 ALTER TABLE `user_reply` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-08 13:51:52
