from flask import Blueprint, request
from controller.post_control import *

route_post = Blueprint("post_route", __name__, url_prefix="/api") 

@route_post.route("/user/<username>/post", methods=['GET', 'POST'])
def postByUsername(username):
    if request.method == 'GET':
        return showPostByUsername(username)
    elif request.method == 'POST':
        return insertPostByUsername(username)

@route_post.route("/user/<username>/post/<int:id>", methods=['GET', 'PUT', 'DELETE'])
def postByUsernameAndPostid(username, id):
    if request.method == 'GET':
        return showPostByPostid(username, id)
    elif request.method == 'DELETE':
        return updatePostByPostid(id)
    elif request.method == 'PUT':
        return deletePostByPostid(id)
