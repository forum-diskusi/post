from flask import Blueprint, request
from controller.user_control import *

route_user = Blueprint("user_route", __name__, url_prefix="/api") 

@route_user.route("/users", methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        return showUsers()
    elif request.method == 'POST':
        return insertUser()

@route_user.route("/user/<username>", methods=['GET', 'DELETE', 'PUT'])
def userByUsername(username):
    if request.method == 'GET':
        return showUserByUsername(username)
    elif request.method == 'DELETE':
        return deleteUserByUsername(username)
    elif request.method == 'PUT':
        return updateUserByUsername(username)
